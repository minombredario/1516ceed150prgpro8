package org.ceedcv.ceed150prgpro8.controlador;

import org.ceedcv.ceed150prgpro8.modelo.IModelo;
import org.ceedcv.ceed150prgpro8.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro8.modelo.Paciente;
import org.ceedcv.ceed150prgpro8.modelo.Medico;
import org.ceedcv.ceed150prgpro8.modelo.Cita;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaCita;
import org.ceedcv.ceed150prgpro8.vista.VerificarCampos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro8.modelo.ModeloMysql;
import org.ceedcv.ceed150prgpro8.vista.VistaTerminal;

/**
 * Fichero: ControladorCita.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public class ControladorCita implements ActionListener{
     
    VistaGraficaCita vgc;
    IModelo modelo;
    String opcion;
    ArrayList citas;
    Cita activa;
    VistaTerminal vt;
    ModeloMysql msql = new ModeloMysql();
    int contActiva;
    //ModeloFichero mf;
    int id_medico, edad_medico, telefono_medico;
    String nombre_medico, dni_medico, colegiado, especialidad, datos_medico;
    int id_paciente, edad_paciente, telefono_paciente;
    String nombre_paciente, dni_paciente, nss, datos_paciente;
    
    public ControladorCita (VistaGraficaCita vistacita , IModelo modelo) throws IOException{
        
        this.vgc = vistacita;
        this.modelo = modelo;
        
        vistacita = new VistaGraficaCita();
        
        this.vgc.getVentana().setVisible(true);
        this.vgc.getBotonCreate().addActionListener(this);
        this.vgc.getBotonRead().addActionListener(this);
        this.vgc.getBotonUpdate().addActionListener(this);
        this.vgc.getBotonDelete().addActionListener(this);
        this.vgc.getBotonCancelar().addActionListener(this);
        this.vgc.getBotonAceptar().addActionListener(this);
        this.vgc.getBotonSalir().addActionListener(this);
        this.vgc.getBotonGuardar().addActionListener(this);
        this.vgc.getBotonBorrar().addActionListener(this);
        this.vgc.getBotonPrimero().addActionListener(this);
        this.vgc.getBotonUltimo().addActionListener(this);
        this.vgc.getBotonSiguiente().addActionListener(this);
        this.vgc.getBotonAnterior().addActionListener(this);
        this.vgc.getBotonMedico().addActionListener(this);
        this.vgc.getBotonPaciente().addActionListener(this);
        
              
        this.editarCampos(Boolean.valueOf(false));
        this.activarBotones(Boolean.valueOf(true));
        
        ComboCita(null);
        
        this.citas = this.modelo.readc();
        if (this.citas.size() > 0) {
          primero();
          mostrar(this.activa);
        }
       
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        
        if (this.vgc.getBotonPrimero() == evento){
            primero();
            mostrar(this.activa);
            
        }else if (this.vgc.getBotonAnterior() == evento){
            anterior();
            mostrar(this.activa);
            
        }else if (this.vgc.getBotonSiguiente() == evento){
            siguiente();
            mostrar(this.activa);
            
        }else if (this.vgc.getBotonUltimo() == evento){
            ultimo();
            mostrar(this.activa);
            
        }else if(this.vgc.getBotonCreate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgc.getBotonFecha().requestFocus();//de esta forma el puntero de editar empieza aqui
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vgc.getTxtId().setEditable(false);
            activarBotones(Boolean.valueOf(false));
            vgc.getBotonMedico().setVisible(true);
            vgc.getBotonPaciente().setVisible(true);
            //VerificarCampos();
            this.opcion = "create";
            
        }else if (vgc.getBotonRead() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgc.getTxtId().requestFocus();
            vaciarCampos();
            vgc.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            
            this.opcion = "read";   
                
        }else if(vgc.getBotonUpdate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgc.getTxtId().requestFocus();
            vaciarCampos();
            vgc.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            
            //VerificarCampos();
            
            this.opcion = "update";  
                
        }else if(vgc.getBotonDelete() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgc.getTxtId().requestFocus();
            vaciarCampos();
            vgc.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
          
        }else if (vgc.getBotonCancelar() == evento){
            
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            vgc.getBotonPaciente().setVisible(false);
            vgc.getBotonMedico().setVisible(false);
            vgc.getBotonGuardar().setVisible(false);
            vgc.getBotonAceptar().setVisible(true);
            primero();
            mostrar(this.activa);
           
        }else if (vgc.getBotonAceptar() == evento){
            menuAceptar(opcion);
            vgc.getBotonPaciente().setVisible(true);
            vgc.getBotonMedico().setVisible(true);
            
        }else if (vgc.getBotonGuardar() == evento){
            Cita cita = new Cita();
            cita = obtener();
            modelo.update(cita);
            citas = modelo.readc();
            
            vgc.getBotonGuardar().setVisible(false);
            vgc.getBotonAceptar().setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            this.activa = cita;
            
        }else if (vgc.getBotonBorrar() == evento){
            Cita cita = new Cita();
            cita = obtener();
            modelo.delete(cita);
            citas = modelo.readc();
            vaciarCampos();
            vgc.getBotonBorrar().setVisible(false);
            vgc.getBotonAceptar().setVisible(true);
            
            anterior();
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
             
        }else if (vgc.getBotonMedico() == evento){
            
            if (!this.vgc.getComboMedico().getSelectedItem().equals("No existe")){
                Cita cita = new Cita();
               cita.setMedico((Medico) vgc.getComboMedico().getSelectedItem());
               id_medico = cita.getMedico().getId();
               nombre_medico = cita.getMedico().getNombre();
               dni_medico = cita.getMedico().getDni();
               colegiado = cita.getMedico().getNcolegiado();
               especialidad = cita.getMedico().getEspecialidad();
               edad_medico = cita.getMedico().getEdad();
               telefono_medico = cita.getMedico().getTelefono();
               datos_medico = "Id: "          + id_medico    + "\nNombre: "       + nombre_medico 
                          + "\nDni: "         + dni_medico   + "\nNº Colegiado: " + colegiado 
                          + "\nEspecialida: " + especialidad + "\nEdad: "         + edad_medico 
                          + "\nTelefono: "    + telefono_medico;
                
                vt.info2(datos_medico , "Datos del medico",new ImageIcon(getClass().getResource("/imagenes/doctor.png")));
            }
        }else if (vgc.getBotonPaciente() == evento){
            
            if (!this.vgc.getComboPaciente().getSelectedItem().equals("No existe")){
                Cita cita = new Cita();
                cita.setPaciente((Paciente) vgc.getComboPaciente().getSelectedItem());
                id_paciente = cita.getPaciente().getId();
                nombre_paciente = cita.getPaciente().getNombre();
                dni_paciente = cita.getPaciente().getDni();
                nss = cita.getPaciente().getNss();
                edad_paciente = cita.getPaciente().getEdad();
                telefono_paciente = cita.getPaciente().getTelefono();
                datos_paciente = "Id: "          + id_paciente    + "\nNombre: "       + nombre_paciente
                           + "\nDni: "         + dni_paciente  + "\nNº Seguridad Social: " + nss 
                           + "\nEdad: "         + edad_paciente
                           + "\nTelefono: "    + telefono_paciente;
                vt.info2(datos_paciente , "Datos del paciente",new ImageIcon(getClass().getResource("/imagenes/paciente.png"))); 
               
            }
           
        }else if (vgc.getBotonSalir() == evento){
            
            this.vgc.getVentana().dispose();
            
            }
        
    }
    public void menuAceptar(String opcion) {
       Cita cita = new Cita();         
        switch(opcion){
            case "create":
                
                if(//!vistacita.getTxtId().getText().equals("") && 
                   !vgc.getBotonFecha().getDate().equals("") /*&& 
                   !vgc.getTxtHora().getText().equals("")*/){
                        cita = obtener();
                        modelo.create(cita);
                        citas.add(cita);
                        mostrar(cita);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                        this.activa = cita;

                }else vt.error("Completa todos los datos");
                
                break;
            case "read":
                if (vgc.getTxtId().getText() != ("")){
                    int idcita = Integer.parseInt(vgc.getTxtId().getText());
                    cita.setId(idcita);
                        if( (idcita -1) > citas.size()){
                            CancelarRead();
                        }else{
                            rellenarDatos(msql.getCita(cita));
                            editarCampos(Boolean.valueOf(false));
                            activarBotones(Boolean.valueOf(true)); 
                        }
               }
               
               break;
            case "update":
                if (vgc.getTxtId().getText() != ("")){
                    int idcita = Integer.parseInt(vgc.getTxtId().getText());
                    cita.setId(idcita);
                        if( (idcita -1) > citas.size()){
                            CancelarRead();
                        }else{
                            rellenarDatos(msql.getCita(cita));
                            vgc.getBotonGuardar().setVisible(true);
                            editarCampos(Boolean.valueOf(true));
                            vgc.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgc.getBotonAceptar().setVisible(false);
                        }
                }
                
                break;
            case "delete":
                if (vgc.getTxtId().getText() != ("")){
                    int idcita = Integer.parseInt(vgc.getTxtId().getText());
                    cita.setId(idcita);
                        if( (idcita -1) > citas.size()){
                            CancelarRead();
                        }else{
                            rellenarDatos(msql.getCita(cita));
                            vgc.getBotonBorrar().setVisible(true);
                            editarCampos(Boolean.valueOf(false));
                            vgc.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgc.getBotonAceptar().setVisible(false);
                        }
                    }
                break;
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vgc.getBotonCreate().setEnabled(booleano.booleanValue());
        this.vgc.getBotonRead().setEnabled(booleano.booleanValue());
        this.vgc.getBotonUpdate().setEnabled(booleano.booleanValue());
        this.vgc.getBotonDelete().setEnabled(booleano.booleanValue());
        this.vgc.getBotonCancelar().setEnabled(!booleano.booleanValue());
        this.vgc.getBotonAceptar().setEnabled(!booleano.booleanValue());
        this.vgc.getBotonMedico().setEnabled(!booleano.booleanValue());
        this.vgc.getBotonPaciente().setEnabled(!booleano.booleanValue());
        
        
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vgc.getTxtId().setEditable(booleano.booleanValue());
        this.vgc.getBotonFecha().setEnabled(booleano.booleanValue());
        //this.vgc.getTxtHora().setEditable(booleano.booleanValue());
        this.vgc.getTxtObservaciones().setEditable(booleano.booleanValue());
        this.vgc.getComboPaciente().setEnabled(booleano.booleanValue());
        this.vgc.getComboMedico().setEnabled(booleano.booleanValue());
        this.vgc.getComboHora().setEnabled(booleano.booleanValue());
              
    }
    
     public void vaciarCampos (){
        this.vgc.getTxtId().setText("");
        this.vgc.getBotonFecha().setDate(null);
        //this.vgc.getTxtHora().setText("");
        this.vgc.getTxtObservaciones().setText("");
        this.vgc.getComboMedico().setSelectedItem(null);
        this.vgc.getComboPaciente().setSelectedItem(null);
        this.vgc.getComboHora().setSelectedItem(null);
           
    }
     
    private Cita obtener() {
        Medico medico = new Medico();
        Paciente paciente = new Paciente();
        Cita cita = new Cita();
        
        int id = 0;
            
           try{
                  id = Integer.parseInt(this.vgc.getTxtId().getText());
                  cita.setId(id);
                }catch (NumberFormatException ex){
                  //cita.setId(0);
                }
           
            cita.setDia(this.vgc.getBotonFecha().getDate());
            //cita.setHora(this.vgc.getTxtHora().getText());
            if(this.vgc.getTxtObservaciones().getText().equals("")){
                cita.setObservaciones(" ");
            }else{
            cita.setObservaciones(this.vgc.getTxtObservaciones().getText());
            }
            
            if (!this.vgc.getComboMedico().getSelectedItem().equals("No existe")){
               cita.setMedico((Medico) vgc.getComboMedico().getSelectedItem());
               
            }else vt.advertencia("Selecciona un medico"); 
            if (!this.vgc.getComboPaciente().getSelectedObjects().equals("No existe")){
               cita.setPaciente((Paciente) vgc.getComboPaciente().getSelectedItem());
               
            }else vt.advertencia("Selecciona un paciente"); 
            
            if (!this.vgc.getComboHora().getSelectedItem().equals("No existe")){
               cita.setHora(String.valueOf(vgc.getComboHora().getSelectedItem()));
               
            }else vt.advertencia("Selecciona una hora"); 
            
        return cita;
    }

    private Cita rellenarDatos(Cita cita){
        
        this.vgc.getBotonFecha().setDate(cita.getDia());
        this.vgc.getTxtObservaciones().setText(cita.getObservaciones());
        ComboCita(cita);
        
        return cita;  
    }
    
    private void CancelarRead(){
        vt.advertencia("Cita no encontrada");
        vaciarCampos();
        activarBotones(Boolean.valueOf(true));
        editarCampos(Boolean.valueOf(false));
        vgc.getBotonPaciente().setVisible(false);
        vgc.getBotonMedico().setVisible(false);
        vgc.getBotonGuardar().setVisible(false);
        vgc.getBotonAceptar().setVisible(true);
        
        primero();
        mostrar(this.activa);
    }
    private void primero() {
        
        if (this.citas != null){
            this.activa = ((Cita)this.citas.get(this.contActiva));
            this.contActiva = 0;
        }else{
            this.activa = null;
            this.contActiva = -1;
        }
               
    }   
    private void anterior() {
       if (this.contActiva != 0){
            this.contActiva -= 1;
            this.activa = ((Cita)this.citas.get(this.contActiva));
        }
        
    }

    private void siguiente() {
        if (this.contActiva != this.citas.size() - 1){
            this.contActiva += 1;
            this.activa = ((Cita)this.citas.get(this.contActiva));
        }
    }

    private void ultimo() {
        
        this.contActiva = (this.citas.size()-1);
        this.activa = ((Cita)this.citas.get(this.contActiva));
                 
    }
    
    private void mostrar(Cita primera){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        
        if (primera == null) {
           
          return;
        }
        
        ComboCita(primera);
        
        if (primera == null) {
          return;
        }
            
        this.vgc.getTxtId().setText(String.valueOf(primera.getId()));
        this.vgc.getBotonFecha().setDate(primera.getDia());
        this.vgc.getTxtObservaciones().setText(primera.getObservaciones());
    }
   
    private void ComboCita(Cita cita){
        
        ArrayList medicos = this.modelo.readm();
        ArrayList pacientes = this.modelo.readp();
        ArrayList citas = this.modelo.readc();
        String[] horas = vgc.getHoras();
              
       
       
          
        if (medicos != null){
            this.vgc.getComboMedico().removeAllItems();
            this.vgc.getComboMedico().addItem("No existe");
            for (int i = 0; i < medicos.size(); i++){
                this.vgc.getComboMedico().addItem(medicos.get(i));
                
                Medico medico = (Medico)medicos.get(i);
                
                    if ((cita != null) && (cita.getMedico().getId() != 0) && 
                        (medico.getId() == (cita.getMedico()).getId())){
                        this.vgc.getComboMedico().setSelectedItem(medicos.get(i));
                    }
                        
            }
        }
        if (pacientes != null){
            this.vgc.getComboPaciente().removeAllItems();
            this.vgc.getComboPaciente().addItem("No existe");
            for (int i = 0; i < pacientes.size(); i++){
                this.vgc.getComboPaciente().addItem(pacientes.get(i));
                
                Paciente paciente = (Paciente)pacientes.get(i);
                
                    if ((cita != null) && (cita.getPaciente().getId() != 0) && 
                        (paciente.getId() == (cita.getPaciente()).getId())){
                        this.vgc.getComboPaciente().setSelectedItem(pacientes.get(i));
                        
                    }
            }
        }
        if (citas != null){//si el arrayList de citas no esta vacio 
            int h = 0;
            this.vgc.getComboHora().removeAllItems();//borra todos los Items
            this.vgc.getComboHora().addItem("No existe"); //añade "No existe"
            
            for (h = 0; h < horas.length; h++){
               this.vgc.getComboHora().addItem(horas[h]); //recorremos el array de horas y las añado al listado
                
            }
            for (int i = 0; i < citas.size(); i++){//recorremos las citas
                Cita ci = (Cita)citas.get(i);//las guardamos en ci
                
                if((cita!=null) && (cita.getPaciente().getId() != 0) && 
                        (cita.getPaciente().getId() != 0)){
                   this.vgc.getComboHora().setSelectedItem(cita.getHora());//si hay coincidencia selecciona la opcion del listado
                }    
            }
        }
    }
}