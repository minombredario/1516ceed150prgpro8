/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.controlador;

import java.io.IOException;
import org.ceedcv.ceed150prgpro8.modelo.IModelo;
//import org.ceedcv.ceed150prgpro8.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro8.modelo.ModeloMysql;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaMenu;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaPrincipal;



/**
 * Fichero: main.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 14-dic-2015
 */
public class Main {

    public static void main(String[] args) throws IOException {

        //IModelo modelo = new ModeloFichero();
        IModelo modelo = new ModeloMysql();

        //VistaMenu vista = new VistaMenu();
        VistaGraficaPrincipal menugraficaprincipal = new VistaGraficaPrincipal();
        
        //Controlador menu = new Controlador(modelo, vista);
        Controlador menu = new Controlador(modelo, menugraficaprincipal);
    }

}
