package org.ceedcv.ceed150prgpro8.controlador;

import org.ceedcv.ceed150prgpro8.vista.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.ceedcv.ceed150prgpro8.modelo.IModelo;
//import org.ceedcv.ceed150prgpro8.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro8.modelo.ModeloMysql;




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class Controlador implements ActionListener {
  
    VistaTerminal vt = new VistaTerminal();
    IModelo modelo;
    
    VistaGraficaPrincipal vistagrafica;
    VistaGraficaMenu menugrafica;
    VistaGraficaPaciente vgp;
    VistaGraficaMedico vgm;
    VistaGraficaCita vgc;
    VistaGraficaAcerca vga;
    
    
    public Controlador(IModelo modelo, VistaGraficaPrincipal vistagrafica) throws IOException {
        this.vistagrafica = vistagrafica;
        this.modelo = modelo;
        //new ModeloFichero();
        new ModeloMysql();
        
        Portada();
      
        this.menugrafica.getBotonMedico().addActionListener(this);
        this.menugrafica.getBotonPaciente().addActionListener(this);
        this.menugrafica.getBotonCita().addActionListener(this);
        this.menugrafica.getBotonDocumentacion().addActionListener(this);
        this.menugrafica.getBotonAcerca().addActionListener(this);
        this.menugrafica.getBotonSalir().addActionListener(this);
        //items menus Paciente, Medico, Cita
        this.vistagrafica.getItemPaciente().addActionListener(this);
        this.vistagrafica.getItemMedico().addActionListener(this);
        this.vistagrafica.getItemCita().addActionListener(this);
       //itmes menu Herramientas
        this.vistagrafica.getItemAcerca().addActionListener(this);
        this.vistagrafica.getItemBD().addActionListener(this);
        this.vistagrafica.getItemTablas().addActionListener(this);
        //items menu Documentos
        this.vistagrafica.getItemPdf().addActionListener(this);
        this.vistagrafica.getItemWeb().addActionListener(this);
        //Item menu Salir
        this.vistagrafica.getItemSalir().addActionListener(this);
    }
      
    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        
        if(menugrafica.getBotonPaciente() == evento|| vistagrafica.getItemPaciente() == evento){
            if(vgp != null){
                vt.mostrarTexto("ya esta abierta\n");
                
                if(!vgp.getVentana().isVisible()){
                    VistaPaciente();
                }
            }else{
              VistaPaciente();
            }
            
        }else if(menugrafica.getBotonMedico() == evento || vistagrafica.getItemMedico() == evento){
            
            if(vgm != null){
                vt.mostrarTexto("ya esta abierta\n");
                
                if (!vgm.getVentana().isVisible()){
                    VistaMedico();
                }
            }else{
                VistaMedico();
            }
           
        }else if(menugrafica.getBotonCita() == evento || vistagrafica.getItemCita() == evento){
            if(vgc != null){
                vt.mostrarTexto("ya esta abierta\n");
                
                if (!vgc.getVentana().isVisible()){
                    VistaCita();
                }
            }else{
                VistaCita();
            }
            
    //herramientas  
        }else if(vistagrafica.getItemBD() == evento){
            
            CrearBD();
            
        }else if(vistagrafica.getItemTablas() == evento){
            
            CrearTablas();   
        
        }else if(menugrafica.getBotonAcerca() == evento || vistagrafica.getItemAcerca() == evento){
           
            VistaAcerca();
            
        }else if(menugrafica.getBotonDocumentacion() == evento || vistagrafica.getItemPdf() == evento){
            String pdf= "src/main/java/org/ceedcv/ceed150prgpro8/documentacion/documentacion.pdf";
            try {
                File path = new File (pdf);
                Desktop.getDesktop().open(path);
            }catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }else if( vistagrafica.getItemWeb()== evento){
            String url = "https://docs.google.com/document/d/1fZhIldQ0L7OKNU0T9swRUIE1Qri9usMAVm__Qz_5gpM/edit?ts=561d4e27";
            
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    desktop.getDesktop().browse(new URI(url));
                } catch (URISyntaxException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }else if(menugrafica.getBotonSalir() == evento || vistagrafica.getItemSalir() == evento){
           Object [] opciones ={"Aceptar","Cancelar"};
            Component rootPane = null;
            int eleccion = JOptionPane.showOptionDialog(rootPane,"¿Desea cerrar la aplicacion?","Mensaje de Confirmacion",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
                if (eleccion == JOptionPane.YES_OPTION){
            
                    System.exit(0);
                }else{ }
        }
    }
    
    public void Portada(){
        menugrafica = new VistaGraficaMenu();  
       
        //menugrafica.getVentana().setResizable(false);       
        //menugrafica.getVentana().setMaximizable(true);
        //menugrafica.getVentana().setIconifiable(true);
        //menugrafica.getVentana().setClosable(true);
            try{
                
                vistagrafica.getEscritorio().add(menugrafica.getVentana()); 
                menugrafica.getVentana().setVisible(true);
                this.menugrafica.getVentana().setMaximum(true);
            }catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
     public void VistaPaciente(){
        vgp = new VistaGraficaPaciente();
                        
        vistagrafica.getEscritorio().add(vgp.getVentana()); 
        vgp.getVentana().setVisible(true);
        //vgp.getVentana().setResizable(false);       
        //vgp.getVentana().setMaximizable(true);
        //vgp.getVentana().setIconifiable(true);
        //vgp.getVentana().setClosable(true);
            try {
                ControladorPaciente cp = new ControladorPaciente(vgp, modelo);
                vgp.getVentana().setMaximum(true);
                vgp.getVentana().setSelected(true);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }  
    }
    
    public void VistaMedico(){
        
        vgm = new VistaGraficaMedico();
        
        vistagrafica.getEscritorio().add(vgm.getVentana()); 
        vgm.getVentana().setVisible(true);
        //vgp.getVentana().setResizable(false);       
        //vgp.getVentana().setMaximizable(true);
        //vgp.getVentana().setIconifiable(true);
        //vgp.getVentana().setClosable(true);
            try {
                ControladorMedico cm = new ControladorMedico(vgm, modelo);
                vgm.getVentana().setMaximum(true);
                vgm.getVentana().setSelected(true);
                }catch(PropertyVetoException ex){
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }catch(IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public void VistaCita(){
        
        vgc = new VistaGraficaCita();  
       
        vistagrafica.getEscritorio().add(vgc.getVentana()); 
        vgc.getVentana().setVisible(true);
        //vgp.getVentana().setResizable(false);       
        //vgp.getVentana().setMaximizable(true);
        //vgp.getVentana().setIconifiable(true);
        //vgp.getVentana().setClosable(true);
            try {
                ControladorCita cc = new ControladorCita(vgc, modelo);
                vgc.getVentana().setMaximum(true);
                vgc.getVentana().setSelected(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    public void VistaAcerca(){
        
        vga = new VistaGraficaAcerca();  
                
        vistagrafica.getEscritorio().add(vga.getVentana()); 
        vga.getVentana().setVisible(true);
        //vgp.getVentana().setResizable(false);       
        //vgp.getVentana().setMaximizable(true);
        //vgp.getVentana().setIconifiable(true);
        //vgp.getVentana().setClosable(true);
            try {
                ControladorAcerca ca = new ControladorAcerca(vga, modelo);
                vga.getVentana().setMaximum(true);
                vga.getVentana().setSelected(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    private void CrearBD() {
        
        ModeloMysql mysql = new ModeloMysql();
        String bd = mysql.CrearBaseDatos();
        
        if (bd != null) {
             vt.error("Ha ocurrido un error durante la instalación \n "
                  + "Error: " + bd);
        }
    }

    private void CrearTablas() {
        ModeloMysql mysql = new ModeloMysql();
        String tablas = mysql.CrearTablas();
        
        if (tablas != null) {
             vt.error("Ha ocurrido un error durante la creacion de las tablas \n "
                   + "Error: " + tablas);
        }
    }

}
   

