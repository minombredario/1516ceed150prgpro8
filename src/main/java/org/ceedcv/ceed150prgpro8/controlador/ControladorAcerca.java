/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import org.ceedcv.ceed150prgpro8.modelo.IModelo;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaAcerca;

/**
 *
 * @author usu1601
 */
public class ControladorAcerca implements ActionListener{
    IModelo modelo;
    VistaGraficaAcerca vga;
    
    public ControladorAcerca (VistaGraficaAcerca vga , IModelo modelo) throws IOException{
        
        this.vga = vga;
        this.modelo = modelo;
        
        this.vga.getVentana().setVisible(true);
        this.vga.getBotonSalir().addActionListener(this);
       
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (vga.getBotonSalir() == e.getSource()){
           vga.getVentana().dispose(); 
        }
    }
}


