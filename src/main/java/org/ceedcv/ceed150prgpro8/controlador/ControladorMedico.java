
package org.ceedcv.ceed150prgpro8.controlador;

import org.ceedcv.ceed150prgpro8.modelo.IModelo;
import org.ceedcv.ceed150prgpro8.modelo.Medico;
import org.ceedcv.ceed150prgpro8.vista.VerificarCampos;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaMedico;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro8.modelo.ModeloMysql;
import org.ceedcv.ceed150prgpro8.vista.VistaTerminal;


/**
 * Fichero: ControladorMedico.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public class ControladorMedico implements ActionListener{

   
    VistaGraficaMedico vgm;
    VistaTerminal vt;
    IModelo modelo;
    String opcion;
    ArrayList medicos;
    Medico activo;
    int contActivo;
    ModeloMysql msql = new ModeloMysql();
    
    
    public ControladorMedico(VistaGraficaMedico vgm, IModelo modelo) throws IOException {
        this.vgm = vgm;
        this.modelo = modelo;
        
        this.vgm.getVentana().setVisible(true);
        this.vgm.getBotonCreate().addActionListener(this);
        this.vgm.getBotonRead().addActionListener(this);
        this.vgm.getBotonUpdate().addActionListener(this);
        this.vgm.getBotonDelete().addActionListener(this);
        this.vgm.getBotonCancelar().addActionListener(this);
        this.vgm.getBotonAceptar().addActionListener(this);
        this.vgm.getBotonSalir().addActionListener(this);
        this.vgm.getBotonGuardar().addActionListener(this);
        this.vgm.getBotonBorrar().addActionListener(this);
        this.vgm.getBotonPrimero().addActionListener(this);
        this.vgm.getBotonUltimo().addActionListener(this);
        this.vgm.getBotonSiguiente().addActionListener(this);
        this.vgm.getBotonAnterior().addActionListener(this);
        
                
        editarCampos(Boolean.valueOf(false));
        
       
        
        this.medicos = this.modelo.readm();
        if (this.medicos.size() > 0) {
          primero();
          mostrar(this.activo);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        if (this.vgm.getBotonPrimero() == evento){
            primero();
            mostrar(this.activo);
            
        }else if (this.vgm.getBotonAnterior() == evento){
            anterior();
            mostrar(this.activo);
            
        }else if (this.vgm.getBotonSiguiente() == evento){
            siguiente();
            mostrar(this.activo);
            
        }else if (this.vgm.getBotonUltimo() == evento){
            ultimo();
            mostrar(this.activo);
            
        }else if(this.vgm.getBotonCreate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgm.getTxtNombre().requestFocus();//de esta forma el puntero de editar empieza aqui
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vgm.getTxtId().setEditable(false);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
           
            this.opcion = "create";
            
        }else if (vgm.getBotonRead()== evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgm.getTxtId().requestFocus();
            vaciarCampos();
            vgm.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "read";   
                
        }else if(vgm.getBotonUpdate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgm.getTxtId().requestFocus();
            vaciarCampos();
            vgm.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            
            this.opcion = "update";  
                
        }else if(vgm.getBotonDelete() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgm.getTxtId().requestFocus();
            vaciarCampos();
            vgm.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
          
        }else if (vgm.getBotonCancelar() == evento){
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            vgm.getBotonGuardar().setVisible(false);
            vgm.getBotonAceptar().setVisible(true);
            editarCampos(Boolean.valueOf(false));
            primero();
            mostrar(this.activo);
            
           
        }else if (vgm.getBotonAceptar() == evento){
            menuAceptar(opcion);
            
        }else if (vgm.getBotonGuardar() == evento){
            
            Medico medico = new Medico();
            medico = obtener();
            //medico.setId(vistamedico.txtId.getText());
            modelo.update(medico);
            medicos = modelo.readm();
            vgm.getBotonGuardar().setVisible(false);
            vgm.getBotonAceptar().setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
            this.activo = medico;
            mostrar(this.activo);
            
        }else if (vgm.getBotonBorrar() == evento){
            
            Medico medico = new Medico();
            medico = obtener();
            modelo.delete(medico);
            medicos = modelo.readm();
            vaciarCampos();
            vgm.getBotonBorrar().setVisible(false);
            vgm.getBotonAceptar().setVisible(true);
            
            anterior();
            mostrar(this.activo);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
        }else if (vgm.getBotonSalir() == evento){
            
            this.vgm.getVentana().dispose();
            
        }
    }
    public void menuAceptar(String opcion) {
        Medico medico = new Medico();
        int idmedico;
        switch(opcion){
            case "create":
                if(//!vistamedico.txtId.getText().equals("") && 
                   !vgm.getTxtNombre().getText().equals("") && 
                   !vgm.getTxtDni().getText().equals("") && 
                   !vgm.getTxtNcolegiado().getText().equals("") && 
                   !vgm.getTxtEspecialidad().getText().equals("") && 
                   !vgm.getTxtEdad().getText().equals("") && 
                   !vgm.getTxtTelefono().getText().equals("")){
                        medico = obtener();
                        modelo.create(medico);
                        medicos.add(medico);
                        mostrar(medico);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                        this.activo = medico;

                }else vt.error("Completa todos los datos");
                break;
            case "read":
                if (vgm.getTxtId().getText() != ("")){
                    idmedico = Integer.parseInt(vgm.getTxtId().getText());
                    medico.setId(idmedico);
                    if(idmedico>medicos.size()){
                        CancelarRead();
                    }else{
                            rellenarDatos(msql.getMedico(medico));
                            editarCampos(Boolean.valueOf(false));
                            activarBotones(Boolean.valueOf(true));
                        }
                } 
                break;
            case "update":
                if (vgm.getTxtId().getText() != ("")){
                    idmedico = Integer.parseInt(vgm.getTxtId().getText());
                    medico.setId(idmedico);
                        if(idmedico > medicos.size()){
                           CancelarRead();
                        }else{
                            rellenarDatos(msql.getMedico(medico));
                            vgm.getBotonGuardar().setVisible(true);
                            editarCampos(Boolean.valueOf(true));
                            vgm.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgm.getBotonAceptar().setVisible(false);
                        }
                }
                break;
            case "delete":
                if (vgm.getTxtId().getText() != ("")){
                    idmedico = Integer.parseInt(vgm.getTxtId().getText());
                    medico.setId(idmedico);
                        if(idmedico > medicos.size()){
                           CancelarRead();
                        }else{
                            rellenarDatos(msql.getMedico(medico));
                            vgm.getBotonBorrar().setVisible(true);
                            editarCampos(Boolean.valueOf(false));
                            vgm.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgm.getBotonAceptar().setVisible(false);
                        }
                }
                break;
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vgm.getBotonCreate().setEnabled(booleano.booleanValue());
        this.vgm.getBotonRead().setEnabled(booleano.booleanValue());
        this.vgm.getBotonUpdate().setEnabled(booleano.booleanValue());
        this.vgm.getBotonDelete().setEnabled(booleano.booleanValue());
        this.vgm.getBotonPrimero().setEnabled(booleano.booleanValue());
        this.vgm.getBotonSiguiente().setEnabled(booleano.booleanValue());
        this.vgm.getBotonAnterior().setEnabled(booleano.booleanValue());
        this.vgm.getBotonUltimo().setEnabled(booleano.booleanValue());
        this.vgm.getBotonCancelar().setEnabled(!booleano.booleanValue());
        this.vgm.getBotonAceptar().setEnabled(!booleano.booleanValue());
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vgm.getTxtId().setEditable(booleano.booleanValue());
        this.vgm.getTxtNombre().setEditable(booleano.booleanValue());
        this.vgm.getTxtDni().setEditable(booleano.booleanValue());
        this.vgm.getTxtNcolegiado().setEditable(booleano.booleanValue());
        this.vgm.getTxtEspecialidad().setEditable(booleano.booleanValue());
        this.vgm.getTxtEdad().setEditable(booleano.booleanValue());
        this.vgm.getTxtTelefono().setEditable(booleano.booleanValue());
        this.vgm.getTxtObservaciones().setEditable(booleano.booleanValue());
        
              
    }
    
     public void vaciarCampos (){
        this.vgm.getTxtId().setText("");
        this.vgm.getTxtNombre().setText("");
        this.vgm.getTxtDni().setText("");
        this.vgm.getTxtNcolegiado().setText("");
        this.vgm.getTxtEspecialidad().setText("");
        this.vgm.getTxtEdad().setText("");
        this.vgm.getTxtTelefono().setText("");
        this.vgm.getTxtObservaciones().setText("");
           
    }
     
    private Medico obtener() {
       
        Medico medico = new Medico();
        int edad, telefono, id;
            try{
                id = Integer.parseInt(this.vgm.getTxtId().getText());
                medico.setId(id);
            }catch (NumberFormatException ex){
                //paciente.setId(0);
            }
            
            medico.setNombre(this.vgm.getTxtNombre().getText());
            medico.setDni(this.vgm.getTxtDni().getText());
            medico.setNcolegiado(this.vgm.getTxtNcolegiado().getText());
            medico.setEspecialidad(this.vgm.getTxtEspecialidad().getText());
            
            if(this.vgm.getTxtObservaciones().getText().equals("")){
                medico.setObservaciones(" ");
                }else{
                     medico.setObservaciones(this.vgm.getTxtObservaciones().getText());
                }  

            try{
                edad = Integer.parseInt(this.vgm.getTxtEdad().getText());
                medico.setEdad(edad);
            }catch (NumberFormatException ex){
                medico.setEdad(0);
            }

            try{
                telefono =Integer.parseInt(this.vgm.getTxtTelefono().getText());
                medico.setTelefono(telefono);
            }catch (NumberFormatException ex){
                medico.setTelefono(000000000);
            }

        return medico;

    }

    private Medico rellenarDatos(Medico me){
        
        this.vgm.getTxtNombre().setText(me.getNombre());
        this.vgm.getTxtDni().setText(me.getDni());
        this.vgm.getTxtNcolegiado().setText(me.getNcolegiado());
        this.vgm.getTxtEspecialidad().setText(me.getEspecialidad());
        this.vgm.getTxtEdad().setText(String.valueOf(me.getEdad()));
        this.vgm.getTxtTelefono().setText(String.valueOf(me.getTelefono()));
        this.vgm.getTxtObservaciones().setText(me.getObservaciones());
        
      return me;  
    }
    private void CancelarRead(){
        vt.advertencia("Medico no encontrado");
        vaciarCampos();
        activarBotones(Boolean.valueOf(true));
        vgm.getBotonGuardar().setVisible(false);
        vgm.getBotonAceptar().setVisible(true);
        editarCampos(Boolean.valueOf(false));
        primero();
        mostrar(this.activo);
    }
    
    private void primero() {
     
        if (this.medicos != null){
            this.activo = ((Medico)this.medicos.get(this.contActivo));
            this.contActivo = 0;
        }else{
            this.activo = null;
            this.contActivo = -1;
        }
    }
    private void anterior() {
       if (this.contActivo != 0){
            this.contActivo -= 1;
            this.activo = ((Medico)this.medicos.get(this.contActivo));
        }
        
    }

    private void siguiente() {
        if (this.contActivo != this.medicos.size() - 1){
            this.contActivo += 1;
            this.activo = ((Medico)this.medicos.get(this.contActivo));
        }
    }

    private void ultimo() {
        
        this.contActivo = (this.medicos.size()-1);
        this.activo = ((Medico)this.medicos.get(this.contActivo));
                 
    }
    
    private void mostrar(Medico primero){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        if (primero == null) {
          return;
        } else {
            
            this.vgm.getTxtId().setText(String.valueOf(primero.getId()));
            this.vgm.getTxtNombre().setText(primero.getNombre());
            this.vgm.getTxtDni().setText(primero.getDni());
            this.vgm.getTxtNcolegiado().setText(primero.getNcolegiado());
            this.vgm.getTxtEspecialidad().setText(primero.getEspecialidad());
            this.vgm.getTxtEdad().setText(String.valueOf(primero.getEdad()));
            this.vgm.getTxtTelefono().setText(String.valueOf(primero.getTelefono()));
            this.vgm.getTxtObservaciones().setText(primero.getObservaciones());
                    
        }
    }
    
    private void VerificarCampos(){
        this.vgm.getTxtEdad().setInputVerifier(new VerificarCampos("txtEdad"));
        this.vgm.getTxtDni().setInputVerifier(new VerificarCampos("txtDni"));
        this.vgm.getTxtNcolegiado().setInputVerifier(new VerificarCampos("txtNcolegiado"));
        this.vgm.getTxtTelefono().setInputVerifier(new VerificarCampos("txtTelefono"));
        this.vgm.getTxtEspecialidad().setInputVerifier(new VerificarCampos("txtEspecialidad"));
        this.vgm.getTxtNombre().setInputVerifier(new VerificarCampos("txtNombre"));
    } 
    
}