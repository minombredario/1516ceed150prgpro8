package org.ceedcv.ceed150prgpro8.controlador;


import org.ceedcv.ceed150prgpro8.vista.VistaGraficaPaciente;
import org.ceedcv.ceed150prgpro8.vista.VerificarCampos;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.event.DocumentListener;
import org.ceedcv.ceed150prgpro8.modelo.IModelo;
import org.ceedcv.ceed150prgpro8.modelo.ModeloMysql;
import org.ceedcv.ceed150prgpro8.modelo.Paciente;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaMenu;
import org.ceedcv.ceed150prgpro8.vista.VistaGraficaPrincipal;
import org.ceedcv.ceed150prgpro8.vista.VistaTerminal;




public class ControladorPaciente implements ActionListener{

    VistaGraficaMenu menugrafica;
    VistaGraficaPaciente vgp;
    VistaGraficaPrincipal vistagrafica;
    VistaTerminal vt;
    IModelo modelo;
    String opcion;
    ArrayList pacientes;
    Paciente activo;
    int contActivo;  
    
    
    ControladorPaciente(VistaGraficaPaciente vgp, IModelo modelo) throws IOException {
        this.vgp = vgp;
        this.modelo = modelo;
       
        //inicializar botones
        this.vgp.getBotonCreate().addActionListener(this);
        this.vgp.getBotonRead().addActionListener(this);
        this.vgp.getBotonUpdate().addActionListener(this);
        this.vgp.getBotonDelete().addActionListener(this);
        this.vgp.getBotonCancelar().addActionListener(this);
        this.vgp.getBotonAceptar().addActionListener(this);
        this.vgp.getBotonSalir().addActionListener(this);
        this.vgp.getBotonGuardar().addActionListener(this);
        this.vgp.getBotonBorrar().addActionListener(this);
        this.vgp.getBotonPrimero().addActionListener(this);
        this.vgp.getBotonUltimo().addActionListener(this);
        this.vgp.getBotonSiguiente().addActionListener(this);
        this.vgp.getBotonAnterior().addActionListener(this);
                
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        
        this.pacientes = this.modelo.readp();
        if (this.pacientes.size() > 0) {
          primero();
          mostrar(this.activo);
        }
    }

    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        if (this.vgp.getBotonPrimero() == evento){
            primero();
            mostrar(this.activo);
            
        }else if (this.vgp.getBotonAnterior() == evento){
            anterior();
            mostrar(this.activo);
            
        }else if (this.vgp.getBotonSiguiente() == evento){
            siguiente();
            mostrar(this.activo);
            
        }else if (this.vgp.getBotonUltimo() == evento){
            ultimo();
            mostrar(this.activo);
            
        }else if(this.vgp.getBotonCreate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgp.getTxtNombre().requestFocus();
            vaciarCampos();
            editarCampos(Boolean.valueOf(true));
            vgp.getTxtId().setEditable(false);
            activarBotones(Boolean.valueOf(false));
            VerificarCampos();
            this.opcion = "create";
            
        }else if (vgp.getBotonRead() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgp.getTxtId().requestFocus();
            vaciarCampos();
            vgp.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "read";   
                
        }else if(vgp.getBotonUpdate() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgp.getTxtId().requestFocus();
            vaciarCampos();
            vgp.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "update";  
                
        }else if(vgp.getBotonDelete() == evento){
//vaciamos campos, hacemos los campos editables y ocultamos el uso de los botones
            vgp.getTxtId().requestFocus();
            vaciarCampos();
            vgp.getTxtId().setEditable(true);
            activarBotones(Boolean.valueOf(false));
            this.opcion = "delete";  
                      
        }else if (vgp.getBotonCancelar() == evento){
            
            vaciarCampos();
            activarBotones(Boolean.valueOf(true));
            vgp.getBotonGuardar().setVisible(false);
            vgp.getBotonAceptar().setVisible(true);
            editarCampos(Boolean.valueOf(false));
            
            primero();
            mostrar(this.activo);
           
        }else if (vgp.getBotonAceptar() == evento){
            menuAceptar(opcion);
            
        }else if (vgp.getBotonGuardar() == evento){
            
            Paciente paciente = new Paciente();
            paciente = obtener();
            modelo.update(paciente);
            pacientes = modelo.readp();
            
            vgp.getBotonGuardar().setVisible(false);
            vgp.getBotonAceptar().setVisible(true);
            
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
            this.activo = paciente;
            mostrar(this.activo);
            
        }else if (vgp.getBotonBorrar() == evento){
            
            Paciente paciente = new Paciente();
            paciente = obtener();
            modelo.delete(paciente);
            pacientes = modelo.readp();
            vaciarCampos();
            vgp.getBotonBorrar().setVisible(false);
            vgp.getBotonAceptar().setVisible(true);
            activarBotones(Boolean.valueOf(true));
            editarCampos(Boolean.valueOf(false));
            
            anterior();
            mostrar(this.activo);
            
        }else if (vgp.getBotonSalir() == evento){
           
            this.vgp.getVentana().dispose();
              
        }
    }
    public void menuAceptar(String opcion) {
        Paciente paciente = new Paciente();
        ModeloMysql msql = new ModeloMysql();
        
        switch(opcion){
            case "create":
                if(//!vistapaciente.txtId.getText().equals("") && 
                   !vgp.getTxtNombre().getText().equals("") && 
                   !vgp.getTxtDni().getText().equals("") && 
                   !vgp.getTxtNss().getText().equals("") && 
                   !vgp.getTxtEdad().getText().equals("") && 
                   !vgp.getTxtTelefono().getText().equals("")){
                        paciente = obtener();
                        modelo.create(paciente);
                        pacientes.add(paciente);
                        mostrar(paciente);
                        editarCampos(Boolean.valueOf(false));
                        activarBotones(Boolean.valueOf(true));
                        this.activo = paciente;
                        mostrar(this.activo );

                }else vt.error("Completa todos los datos");
                break;
            case "read":
                if (vgp.getTxtId().getText() != ("")){
                    int idpaciente = Integer.parseInt(vgp.getTxtId().getText());
                    paciente.setId(idpaciente);
                        if(idpaciente > pacientes.size()){
                           CancelarRead();
                        }else{
                            RellenarDatos(msql.getPaciente(paciente));
                            editarCampos(Boolean.valueOf(false));
                            activarBotones(Boolean.valueOf(true));
                        }
                }
               break;
            case "update":
                if (vgp.getTxtId().getText() != ("")){
                    int idpaciente = Integer.parseInt(vgp.getTxtId().getText());
                        if(idpaciente > pacientes.size()){
                           CancelarRead();
                        }else{
                            RellenarDatos(msql.getPaciente(paciente));
                            vgp.getBotonGuardar().setVisible(true);
                            editarCampos(Boolean.valueOf(true));
                            vgp.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgp.getBotonAceptar().setVisible(false);
                        }
                }
                 break;
            case "delete":
                if (vgp.getTxtId().getText() != ("")){
                    int idpaciente = Integer.parseInt(vgp.getTxtId().getText());
                        if(idpaciente > pacientes.size()){
                           CancelarRead();
                        }else{
                            RellenarDatos(msql.getPaciente(paciente));
                            vgp.getBotonBorrar().setVisible(true);
                            editarCampos(Boolean.valueOf(false));
                            vgp.getTxtId().setEditable(false);
                            activarBotones(Boolean.valueOf(false));
                            vgp.getBotonAceptar().setVisible(false);
                        }
                }
                break;
           
        }
    }
       
    public void activarBotones(Boolean booleano){
        
        this.vgp.getBotonCreate().setEnabled(booleano.booleanValue());
        this.vgp.getBotonRead().setEnabled(booleano.booleanValue());
        this.vgp.getBotonUpdate().setEnabled(booleano.booleanValue());
        this.vgp.getBotonDelete().setEnabled(booleano.booleanValue());
        this.vgp.getBotonPrimero().setEnabled(booleano.booleanValue());
        this.vgp.getBotonSiguiente().setEnabled(booleano.booleanValue());
        this.vgp.getBotonAnterior().setEnabled(booleano.booleanValue());
        this.vgp.getBotonUltimo().setEnabled(booleano.booleanValue());
        this.vgp.getBotonCancelar().setEnabled(!booleano.booleanValue());
        this.vgp.getBotonAceptar().setEnabled(!booleano.booleanValue());
        
    }
    
    public void editarCampos(Boolean booleano){
        
        this.vgp.getTxtId().setEditable(booleano.booleanValue());
        this.vgp.getTxtNombre().setEditable(booleano.booleanValue());
        this.vgp.getTxtDni().setEditable(booleano.booleanValue());
        this.vgp.getTxtNss().setEditable(booleano.booleanValue());
        this.vgp.getTxtEdad().setEditable(booleano.booleanValue());
        this.vgp.getTxtTelefono().setEditable(booleano.booleanValue());
        this.vgp.getTxtObservaciones().setEditable(booleano.booleanValue());
        
              
    }
    
     public void vaciarCampos (){
        this.vgp.getTxtId().setText("");
        this.vgp.getTxtNombre().setText("");
        this.vgp.getTxtDni().setText("");
        this.vgp.getTxtNss().setText("");
        this.vgp.getTxtEdad().setText("");
        this.vgp.getTxtTelefono().setText("");
        this.vgp.getTxtObservaciones().setText("");  
    }
     
    private Paciente obtener() {
       
        Paciente paciente = new Paciente();
        int edad, telefono, id;
                try{
                  id = Integer.parseInt(this.vgp.getTxtId().getText());
                  paciente.setId(id);
                }catch (NumberFormatException ex){
                  //paciente.setId(0);
                }
            
            
        paciente.setNombre(this.vgp.getTxtNombre().getText());
        paciente.setDni(this.vgp.getTxtDni().getText());
        paciente.setNss(this.vgp.getTxtNss().getText());
            
            if(this.vgp.getTxtObservaciones().getText().equals("")){
                paciente.setObservaciones(" ");
            }else{
                paciente.setObservaciones(this.vgp.getTxtObservaciones().getText());
            } 
            
            try{
                edad = Integer.parseInt(this.vgp.getTxtEdad().getText());
                paciente.setEdad(edad);
            }catch (NumberFormatException ex){
                paciente.setEdad(0);
            }

            try{
                telefono =Integer.parseInt(this.vgp.getTxtTelefono().getText());
                paciente.setTelefono(telefono);
            }catch (NumberFormatException ex){
                paciente.setTelefono(000000000);
            }

        return paciente;
    }

   
    private Paciente RellenarDatos(Paciente paciente) {
               
                this.vgp.getTxtNombre().setText(paciente.getNombre());
                this.vgp.getTxtDni().setText(paciente.getDni());
                this.vgp.getTxtNss().setText(paciente.getNss());
                this.vgp.getTxtEdad().setText(String.valueOf(paciente.getEdad()));
                this.vgp.getTxtTelefono().setText(String.valueOf(paciente.getTelefono()));
                this.vgp.getTxtObservaciones().setText(paciente.getObservaciones());
                            
        return paciente;       
    }
    
    private void CancelarRead(){
        vt.advertencia("Paciente no encontrado");
        vaciarCampos();
        activarBotones(Boolean.valueOf(true));
        vgp.getBotonGuardar().setVisible(false);
        vgp.getBotonAceptar().setVisible(true);
        editarCampos(Boolean.valueOf(false));
        primero();
        mostrar(this.activo);
    }
    
    private void primero() {
        
        if (this.pacientes != null){
            this.activo = ((Paciente)this.pacientes.get(this.contActivo));
        } else{
            this.activo = null;
            this.contActivo = -1;
        }
                       
    }   
    private void anterior() {
       if (this.contActivo != 0){
            this.contActivo -= 1;
            this.activo = ((Paciente)this.pacientes.get(this.contActivo));
        }
        
    }

    private void siguiente() {
        if (this.contActivo != this.pacientes.size() - 1){
            this.contActivo += 1;
            this.activo = ((Paciente)this.pacientes.get(this.contActivo));
        }
    }

    private void ultimo() {
        this.contActivo = (this.pacientes.size() - 1);
        this.activo = ((Paciente)this.pacientes.get(this.contActivo));
                 
    }
    
    private void mostrar(Paciente primero){
        
        editarCampos(Boolean.valueOf(false));
        activarBotones(Boolean.valueOf(true));
        if (primero == null) {
          return;
        } else {
            
            this.vgp.getTxtId().setText(String.valueOf(primero.getId()));
            this.vgp.getTxtNombre().setText(primero.getNombre());
            this.vgp.getTxtDni().setText(primero.getDni());
            this.vgp.getTxtNss().setText(primero.getNss());
            this.vgp.getTxtEdad().setText(String.valueOf(primero.getEdad()));
            this.vgp.getTxtTelefono().setText(String.valueOf(primero.getTelefono()));
            this.vgp.getTxtObservaciones().setText(primero.getObservaciones());
        }
    }
    private void VerificarCampos(){
        this.vgp.getTxtEdad().setInputVerifier(new VerificarCampos("txtEdad"));
        this.vgp.getTxtDni().setInputVerifier(new VerificarCampos("txtDni"));
        this.vgp.getTxtNss().setInputVerifier(new VerificarCampos("txtNss"));
        this.vgp.getTxtTelefono().setInputVerifier(new VerificarCampos("txtTelefono"));
        this.vgp.getTxtNombre().setInputVerifier(new VerificarCampos("txtNombre"));
    }
   
}