/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.modelo;

import java.util.ArrayList;


/**
 * Fichero: Imodelo.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public interface IModelo {
    
    //******PACIENTE******

    public void create(Paciente paciente);

    public void update(Paciente paciente);

    public void delete(Paciente paciente);
        
    public ArrayList <Paciente> readp();
    
   
     //******MEDICO******

    public void create(Medico medico);

    public void update(Medico medico);

    public void delete(Medico medico);
    
    public ArrayList <Medico> readm ();
    
    
    //********CITA********
    
    public void create(Cita cita);//Añade una cita al final

    public void update(Cita cita);//Actualiza los datos de la cita

    public void delete(Cita cita);//Borra el objeto seleccionado*/
       
    public ArrayList <Cita> readc ();

   

    
    
}
