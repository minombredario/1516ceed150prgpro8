package org.ceedcv.ceed150prgpro8.modelo;

/**
 * Fichero: Persona.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Medico extends Persona {

    private String ncolegiado;
    private String especialidad;
    private int id = 0;

   public Medico(){
      
   ncolegiado = " ";
   especialidad = " ";
   }
   
   public Medico(int id){
       this.id = id;
   }
    public Medico(int id, String nombre, String dni, int edad, int telefono,
            String observaciones, String ncolegiado, String especialidad){
        super(nombre, dni, edad, telefono, observaciones);
        this.id = id;
        this.ncolegiado = ncolegiado;
        this.especialidad = especialidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
       
    
    /**
     * @return the ncolegiado
     */
    public String getNcolegiado() {
        return ncolegiado;
    }

    /**
     * @param ncolegiado the ncolegiado to set
     */
    public void setNcolegiado(String ncolegiado) {
        this.ncolegiado = ncolegiado;
    }

    /**
     * @return the especialidad
     */
    public String getEspecialidad() {
        return especialidad;
    }

    /**
     * @param especialidad the especialidad to set
     */
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String toString() {
        return  "" + id + ": " + nombre; /* + "\nDni: "+ dni + "\nN. Colegiado: "+ ncolegiado + "\nEspecialidad: " + especialidad + "\nEdad: " + edad +
                "\nTelefono: " + telefono + "\nObservaciones: " + observaciones;*/
    }

}
