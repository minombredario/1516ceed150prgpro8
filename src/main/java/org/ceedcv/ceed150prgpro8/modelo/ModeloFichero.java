/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed150prgpro8.vista.VistaTerminal;



/**
 *
 * @author usu1601
 */
public  class ModeloFichero implements IModelo{
    
    File filepacientes = new File("pacientes.csv");
    File filemedicos = new File("medicos.csv");
    File filecitas = new File("citas.csv");
    int idcountpa;
    int idcountme;
    int idcountci;
    Date dia;
    VistaTerminal vt = new VistaTerminal();
    
    ArrayList pacientes;
    ArrayList medicos;
    ArrayList citas;
    
    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public void create(Paciente paciente) {
        idcountpa = contPaciente()+1;
        paciente.setId(idcountpa);
        try {
            FileWriter filewriter = new FileWriter(filepacientes, true);
            filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getDni()  + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
            filewriter.close();
           
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void update(Paciente paciente) {
        File temp_pa = new File("temp.csv");
        Paciente pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Paciente(id, nombre, dni, edad, telefono,
              observaciones, nss);
                
                if (paciente.getId() == (id)) {
                    filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getDni()  + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
                } else {
                    filewriter.write(
                      pa.getId()   + ";" + pa.getNombre() + ";"
                    + pa.getDni()  + ";"
                    + pa.getNss()  + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad() + ";" + pa.getTelefono() +";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public void delete(Paciente paciente) {
        File temp_pa = new File("temp.csv");
        Paciente pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Paciente(id, nombre, dni, edad, telefono,
              observaciones, nss);
                if (paciente.getId() != id) {
                    filewriter.write(
                      pa.getId()        + ";" + pa.getNombre() + ";"
                    + paciente.getDni() + ";"
                    + pa.getNss()       + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad()      + ";" + pa.getTelefono() +";\r\n");
                } 
                s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public ArrayList<Paciente> readp() {
        
        pacientes = new ArrayList();
        Paciente paciente = new Paciente();
        
        if (filepacientes.exists()){
        try {
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                paciente = new Paciente(id, nombre, dni, edad, telefono,
               nss, observaciones);
                pacientes.add(paciente);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
         }else {vt.mostrarTexto("\n==============================\n"
                           + "  El fichero no esta creado"
                           + "\n==============================\n");
            
            }
        return pacientes;
    }

    @Override
    public void create(Medico medico) {
       idcountme = contMedico()+1;
       medico.setId(idcountme);
        try {
            FileWriter filewriter = new FileWriter(filemedicos, true);
            
            
            filewriter.write(
                    
                      medico.getId()   + ";" + medico.getNombre()     + ";"
                    + medico.getDni()           + ";"
                    + medico.getObservaciones() + ";" + medico.getNcolegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
            filewriter.close();
                
           
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void update(Medico medico) {
        File temp_me = new File("temp.csv");
        Medico doc;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                                
                doc = new Medico(id, nombre, dni, edad, telefono, observaciones, 
                        ncolegiado, especialidad);
                if (medico.getId()==id) {
                    filewriter.write(
                      medico.getId()            + ";" + medico.getNombre()     + ";"
                    + medico.getDni()           + ";"
                    + medico.getObservaciones() + ";" + medico.getNcolegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
                    
                } else {
                    filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getDni()           + ";"
                    + doc.getObservaciones() + ";" + doc.getNcolegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad() + ";" + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public void delete(Medico medico) {
        File temp_me = new File("temp.csv");
        Medico doc = null;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                doc = new Medico(id, nombre, dni, edad, telefono,
                        ncolegiado, especialidad, observaciones);
                if (medico.getId() != id) {
                     filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getDni()           + ";"
                    + doc.getObservaciones() + ";" + doc.getNcolegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad() + ";" + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }
                s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }System.out.print (doc);
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public ArrayList<Medico> readm() {
        medicos = new ArrayList();
        Medico medico = new Medico();
        
        if (filemedicos.exists()){
        try {
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                medico = new Medico(id, nombre, dni, edad, telefono, observaciones,
                        ncolegiado, especialidad );
                medicos.add(medico);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }else {vt.mostrarTexto("\n==============================\n"
                           + "  El fichero no esta creado"
                           + "\n==============================\n");
            
            }
        return medicos;
    }

    @Override
    public void create(Cita cita) {
        
        idcountci = contCita()+1;
        cita.setId(idcountci);
        try {
            FileWriter filewriter = new FileWriter(filecitas, true);
            filewriter.write(
                      cita.getId()               + ";" + sdf.format(cita.getDia())          + ";" 
                    + cita.getHora()             + ";" + cita.getObservaciones() + ";"
                    + cita.getPaciente().getId() + ";" + cita.getMedico().getId() + "\r\n");
            filewriter.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Cita cita) {
        File temp_ci = new File("temp.csv");
        Cita ci;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
                
                Paciente paciente = new  Paciente (idpa);
                Medico medico = new Medico(idme);                                    
                ci = new Cita(id, dia, hora, observaciones, medico, paciente);
                
                if (cita.getId() != (id)) {
                     filewriter.write(
                      cita.getId()               + ";" + sdf.format(cita.getDia())            + ";" 
                    + cita.getHora()             + ";" + cita.getObservaciones() + ";" 
                    + cita.getPaciente().getId() + ";" + cita.getMedico().getId() + ";" 
                    + "\r\n");
                   
                } else {
                    filewriter.write(
                      ci.getId()               + ";" + sdf.format(ci.getDia())            + ";" 
                    + ci.getHora()             + ";" + ci.getObservaciones() + ";" 
                    + ci.getPaciente().getId() + ";" + ci.getMedico().getId() + ";" 
                    + "\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public void delete(Cita cita) {
        File temp_ci = new File("temp.csv");
        Cita ci = null;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
               
                
                Paciente paciente = new  Paciente (idpa);
                Medico medico = new Medico(idme);
                
               
                ci = new Cita(id, dia, hora, observaciones, medico, paciente);
                if (cita.getId() != (id)) {
                     filewriter.write(
                      ci.getId()               + ";" + sdf.format(ci.getDia())            + ";" 
                    + ci.getHora()             + ";" + ci.getObservaciones() + ";" 
                    + ci.getPaciente().getId() + ";" + ci.getMedico().getId() + ";" 
                    + "\r\n");
                }
                                
               s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public ArrayList<Cita> readc() {
        citas = new ArrayList();
        Cita cita = new Cita();
                
        if(filecitas.exists()){
        try {
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
                
               
                Paciente paciente = getPaciente(idpa);
                cita.setPaciente(paciente);
                
                Medico medico = getMedico(idme);
                cita.setMedico(medico);
                
                cita = new Cita(id, dia, hora, observaciones, medico, paciente);
                
                citas.add(cita);
                s = bufereader.readLine();
            } 
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }else {vt.mostrarTexto("\n==============================\n"
                           + "  El fichero no esta creado"
                           + "\n==============================\n");
            }
        return citas;
    }
    
     private Paciente getPaciente(int id) {

        Paciente paciente_cita = null;
        
        Iterator it = readp().iterator();
        while (it.hasNext()) {
            Paciente paciente = (Paciente) it.next();
            if (id == (paciente.getId())) {
                paciente_cita = paciente;
            }
        }
        return paciente_cita;

    }
    
    private Medico getMedico(int id) {

        Medico medico_cita = null;
        
        Iterator it = readm().iterator();
        while (it.hasNext()) {
            Medico medico = (Medico) it.next();
            if (id == (medico.getId())) {
                medico_cita = medico;
            }
        }
        return medico_cita;

    }
    
    private int contMedico(){
        int idmayor = 0;
        Medico medico;
        
            if (this.filemedicos.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filemedicos);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        medico = new Medico(id);
                        medico.setId(id);
                        int idmedico = medico.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idmedico){
                                idmayor = idmedico;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }
    
    
    
    private int contPaciente(){
        int idmayor = 0;
        Paciente paciente;
        
            if (this.filepacientes.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filepacientes);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        paciente = new Paciente(id);
                        paciente.setId(id);
                        int idpaciente = paciente.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idpaciente){
                                idmayor = idpaciente;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }
    private int contCita(){
        int idmayor = 0;
        Cita cita;
        
            if (this.filecitas.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filecitas);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        cita = new Cita(id);
                        cita.setId(id);
                        int idcita = cita.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idcita){
                                idmayor = idcita;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }
  
}
