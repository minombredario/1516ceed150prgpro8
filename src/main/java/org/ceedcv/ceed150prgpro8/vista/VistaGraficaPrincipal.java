/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.vista;

import java.awt.*;
import javax.swing.*;



/**
 *
 * @author usu1601
 */
public class VistaGraficaPrincipal extends JFrame{
    private JMenu MenuPaciente, MenuMedico, MenuCita, MenuHerramientas, MenuDocumentos, MenuSalir;
    private JMenuItem ItemBD, ItemTablas, ItemAcerca, ItemPdf, ItemWeb, ItemCita, ItemPaciente, ItemMedico, ItemSalir;
    private JMenuBar BarraMenu;
    private JPanel panel;
    private JFrame contenedor;
    private JDesktopPane escritorio;
    
      
    public VistaGraficaPrincipal(){
    
        contenedor = new JFrame();
        
        panel = new JPanel();
        escritorio = new JDesktopPane();
        panel.setLayout(null);
        contenedor.setContentPane(panel);
        contenedor.setContentPane(escritorio);
 
        //Creamos la barra de Menu
        BarraMenu=new JMenuBar();
 
        //Creamos los menus
        MenuPaciente = new JMenu("Pacientes");
            ItemPaciente = new JMenuItem("Paciente");
        MenuMedico = new JMenu("Medicos");
            ItemMedico = new JMenuItem("Medico");
	MenuCita = new JMenu("Citas");
            ItemCita = new JMenuItem("Cita");
	MenuHerramientas = new JMenu("Herramientas");
            ItemBD =new JMenuItem("Instalar Base de Datos");
            ItemTablas =new JMenuItem("Crear Tablas en BD");
            ItemAcerca =new JMenuItem("Acerca");
	MenuDocumentos = new JMenu("Documentos");
            ItemPdf = new JMenuItem("Pdf");
            ItemWeb = new JMenuItem("WEB");
        MenuSalir = new JMenu("Salir");
            ItemSalir= new JMenuItem("Salir");
        
        //Añadimos los menus a la barra de menu
        BarraMenu.add(MenuPaciente);
            MenuPaciente.add(ItemPaciente);
        BarraMenu.add(MenuMedico);
            MenuMedico.add(ItemMedico);
        BarraMenu.add(MenuCita);
            MenuCita.add(ItemCita);
        BarraMenu.add(MenuHerramientas);
            MenuHerramientas.add(ItemBD);
            MenuHerramientas.add(ItemTablas);
            MenuHerramientas.add(ItemAcerca);
        BarraMenu.add(MenuDocumentos);
            MenuDocumentos.add(ItemPdf);
            MenuDocumentos.add(ItemWeb);
        BarraMenu.add(MenuSalir);
            MenuSalir.add(ItemSalir);
      /*VistaGraficaMenu vista = new VistaGraficaMenu();  
      escritorio.add(vista.ventana);      */ 
        //Indicamos que es el menu por defecto
        contenedor.setJMenuBar(BarraMenu);
        contenedor.setVisible(true);
        
        //ventana.setLocationRelativeTo(null);
            contenedor.setSize(800,500);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = contenedor.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
        contenedor.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
	contenedor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
    }
    
    /*
    public static void main(String[] args) {
        VistaGraficaPrincipal principal = new VistaGraficaPrincipal();
    }*/

    public JMenu getMenuPaciente() {
        return MenuPaciente;
    }

    public JMenu getMenuMedico() {
        return MenuMedico;
    }

    public JMenu getMenuCita() {
        return MenuCita;
    }

    public JMenu getMenuHerramientas() {
        return MenuHerramientas;
    }

    public JMenu getMenuDocumentos() {
        return MenuDocumentos;
    }

    public JMenu getMenuSalir() {
        return MenuSalir;
    }

    public JMenuItem getItemBD() {
        return ItemBD;
    }

    public JMenuItem getItemTablas() {
        return ItemTablas;
    }
    
    public JMenuItem getItemAcerca() {
        return ItemAcerca;
    }

    public JMenuItem getItemPdf() {
        return ItemPdf;
    }

    public JMenuItem getItemWeb() {
        return ItemWeb;
    }

    public JMenuItem getItemCita() {
        return ItemCita;
    }

    public JMenuItem getItemPaciente() {
        return ItemPaciente;
    }

    public JMenuItem getItemMedico() {
        return ItemMedico;
    }

    public JMenuItem getItemSalir() {
        return ItemSalir;
    }

    public JMenuBar getBarraMenu() {
        return BarraMenu;
    }

    public JPanel getPanel() {
        return panel;
    }

    public JFrame getContenedor() {
        return contenedor;
    }

    public JDesktopPane getEscritorio() {
        return escritorio;
    }

  }