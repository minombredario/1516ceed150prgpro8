/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro8.vista;

import com.toedter.calendar.JDateChooser;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.*;
import org.ceedcv.ceed150prgpro8.modelo.Medico;

/**
* Fichero: VistaGraficaCita.java
* @author Darío Navarro Andrés <minombredario@gmail.com>
* @date 29-feb-2016
*/
public class VistaGraficaCita extends JInternalFrame{
    
    private JLabel etiId, etiFecha, etiHora, etiObservaciones, etiMenu, etiMedico, 
            etiPaciente, etiImagen;
    private JTextField txtId, /*txtFecha,*/ txtHora;
    private  JButton botonCreate, botonUpdate, botonRead, botonDelete, botonSalir,
            botonGuardar,botonCancelar, botonAceptar, botonBorrar, botonPrimero, 
            botonUltimo, botonAnterior, botonSiguiente, botonPaciente, botonMedico;
    private JTextArea txtObservaciones;
    private JInternalFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;
    private JComboBox comboPaciente, comboMedico, comboHora;
    private JDateChooser botonFecha;
    private String[] horas = {
                          "08:00","08:15","08:30","08:45",
                          "09:00","09:15","09:30","09:45",
                          "10:00","10:15","10:30","10:45",
                          "11:00","11:15","11:30","11:45",
                          "12:00","12:15","12:30","12:45", 
                          "13:00","13:15","13:30","13:45", 
                          "15:00","15:15","15:30","15:45", 
                          "16:00","16:15","16:30","16:45", 
                          "17:00","17:15","17:30","17:45", 
                          "18:00","18:15","18:30","18:45", 
                          "18:00","18:15","18:30","18:45", 
                          "19:00","19:15","19:30","19:45", 
                          "20:00","20:15","20:30","20:45"};

    
    public VistaGraficaCita(){
               
        panel = new JPanel();
        panel.setLayout(null);
        //String img = "src/main/resources/imagenes/cita.png";
        
        etiMenu = new JLabel("MENU CITA");
        etiMenu.setBounds(300, 5, 300, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiFecha = new JLabel("Fecha: ");
        etiFecha.setBounds(10, 50, 80, 20);
        etiHora = new JLabel("Hora: ");
        etiHora.setBounds(10, 80, 80, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiMedico = new JLabel ("Medico: ");
        etiMedico.setBounds(170, 50, 80, 20);
        etiPaciente = new JLabel("Paciente: ");
        etiPaciente.setBounds(170, 80, 120, 20);
                       
        //imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/cita.png")));
        etiImagen.setBounds(420, 5, 250, 350);
        
        
        
        txtId = new JTextField();
        txtId.setBounds(55, 20, 100, 20);
        //txtFecha = new JTextField();
        //txtFecha.setBounds(75, 50, 100, 20);
        //txtHora = new JTextField();
        //txtHora.setBounds(55, 80, 100, 20);
        comboHora = new JComboBox(horas);
        comboHora.setBounds(55, 80, 100, 20);
        comboPaciente = new JComboBox();
        comboPaciente.setBounds(230, 80, 200, 20);
        comboMedico = new JComboBox();
        comboMedico.setBounds(230, 50, 200, 20);
       
               
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 425, 150);
        
        botonCreate = new JButton("CREATE");
        botonCreate.setBounds(10, 350, 90, 20);
        botonRead = new JButton("READ");
        botonRead.setBounds(110, 350, 90, 20);
        botonUpdate = new JButton("UPDATE");
        botonUpdate.setBounds(210, 350, 90, 20);
        botonDelete = new JButton("DELETE");
        botonDelete.setBounds(310, 350, 90, 20);
        botonAceptar = new JButton("ACEPTAR");
        botonAceptar.setBounds(550, 350, 100, 20);
        botonCancelar = new JButton("CANCELAR");
        botonCancelar.setBounds(670, 350, 100, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(670, 10, 100, 20);
        botonGuardar = new JButton("GUARDAR");
        botonGuardar.setBounds(550, 350, 100, 20);
        botonGuardar.setVisible(false);
        botonBorrar = new JButton("BORRAR");
        botonBorrar.setBounds(550, 350, 100, 20);
        botonBorrar.setVisible(false);
        
        botonFecha = new JDateChooser();
        botonFecha.setBounds(55, 50, 100, 20);
        
        botonPrimero = new JButton("<<");
        botonPrimero.setBounds(70, 320, 50, 20);
        botonAnterior =new JButton("<");
        botonAnterior.setBounds(140, 320, 50, 20);
        botonSiguiente = new JButton(">");
        botonSiguiente.setBounds(210, 320, 50, 20);
        botonUltimo = new JButton (">>");
        botonUltimo.setBounds(280, 320, 50, 20);
        
        botonPaciente = new JButton("Ver");
        botonPaciente.setBounds(440, 80, 60, 20);
        botonPaciente.setVisible(false);
        botonMedico = new JButton("Ver");
        botonMedico.setBounds(440, 50, 60, 20);
        botonMedico.setVisible(false);
        
        panel.add(etiId);
        panel.add(etiFecha);
        panel.add(etiHora);
        panel.add(etiPaciente);
        panel.add(etiMedico);
        panel.add(etiObservaciones);
        panel.add(txtId);
        //panel.add(txtFecha);
        //panel.add(txtHora);
        panel.add(comboPaciente);
        panel.add(comboMedico);
        panel.add(comboHora);
        
        
        panel.add(txtObservaciones);
        
        
        panel.add(botonCreate);
        panel.add(botonUpdate);
        panel.add(botonRead);
        panel.add(botonDelete);
        panel.add(botonAceptar);
        panel.add(botonSalir);
        panel.add(botonCancelar);
        panel.add(botonGuardar);
        panel.add(botonBorrar);
        
        panel.add(botonPrimero);
        panel.add(botonSiguiente);
        panel.add(botonAnterior);
        panel.add(botonUltimo);
       
        panel.add(botonPaciente);
        panel.add(botonMedico);
        
        panel.add(botonFecha);
        
        panel.add(etiImagen);
        
        
        ventana = new JInternalFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,420);
        //este metodo devuelve el tamaño de la pantalla
            //Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            //Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            //ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
            
        //ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_ENTER, 0));
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_TAB, 0));
        
        // Se pasa el conjunto de teclas al panel principal 
        ventana.getContentPane().setFocusTraversalKeys(
                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, 
                teclas);
        ventana.add(panel);
        
        //ventana.setVisible(true);
    }
    
    /*    
    public static void main(String[] args) {
        VistaGraficaCita principal = new VistaGraficaCita();
    }*/

    public JLabel getEtiId() {
        return etiId;
    }

    public JLabel getEtiFecha() {
        return etiFecha;
    }

    public JLabel getEtiHora() {
        return etiHora;
    }

    public JLabel getEtiObservaciones() {
        return etiObservaciones;
    }

    public JLabel getEtiMenu() {
        return etiMenu;
    }

    public JLabel getEtiMedico() {
        return etiMedico;
    }

    public JLabel getEtiPaciente() {
        return etiPaciente;
    }

    public JLabel getEtiImagen() {
        return etiImagen;
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public JTextField getTxtHora() {
        return txtHora;
    }

    public JButton getBotonCreate() {
        return botonCreate;
    }

    public JButton getBotonUpdate() {
        return botonUpdate;
    }

    public JButton getBotonRead() {
        return botonRead;
    }

    public JButton getBotonDelete() {
        return botonDelete;
    }

    public JButton getBotonSalir() {
        return botonSalir;
    }

    public JButton getBotonGuardar() {
        return botonGuardar;
    }

    public JButton getBotonCancelar() {
        return botonCancelar;
    }

    public JButton getBotonAceptar() {
        return botonAceptar;
    }

    public JButton getBotonBorrar() {
        return botonBorrar;
    }

    public JButton getBotonPrimero() {
        return botonPrimero;
    }

    public JButton getBotonUltimo() {
        return botonUltimo;
    }

    public JButton getBotonAnterior() {
        return botonAnterior;
    }

    public JButton getBotonSiguiente() {
        return botonSiguiente;
    }

    public JButton getBotonPaciente() {
        return botonPaciente;
    }

    public JButton getBotonMedico() {
        return botonMedico;
    }

    public JTextArea getTxtObservaciones() {
        return txtObservaciones;
    }

    public JInternalFrame getVentana() {
        return ventana;
    }

    public JPanel getPanel() {
        return panel;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public JComboBox getComboPaciente() {
        return comboPaciente;
    }

    public JComboBox getComboMedico() {
        return comboMedico;
    }

    public JDateChooser getBotonFecha() {
        return botonFecha;
    }

    public JComboBox getComboHora() {
        return comboHora;
    }

    public String[] getHoras() {
        return horas;
    }
    
}
