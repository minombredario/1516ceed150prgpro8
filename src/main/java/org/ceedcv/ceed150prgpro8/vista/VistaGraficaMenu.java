/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.vista;


import java.awt.*;
import javafx.scene.layout.Border;
import javax.swing.*;
import static javax.swing.text.StyleConstants.Foreground;
    
public class VistaGraficaMenu{
   
    private JButton botonPaciente, botonMedico, botonCita, botonAcerca, botonDocumentacion, botonSalir;
    private JInternalFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;
    private JLabel etiImagen, etiMenu;
   
    public VistaGraficaMenu(){
        
        //String img = "src/main/resources/imagenes/consulta.png";
        Font font = new Font ("helvetica" , Font.BOLD, 25);
        panel = new JPanel();
        panel.setLayout(null);
       
        /*etiMenu = new JLabel("MENU PRINCIPAL");
        etiMenu.setBounds(140, 5, 440, 60);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 50));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);*/
        
               
        botonPaciente = new JButton("PACIENTE");
        botonPaciente.setBounds(55, 300, 180, 60);
        botonPaciente.setFont(font);
        botonPaciente.setToolTipText("Gestionar Paciente");
        botonMedico = new JButton("MEDICO");
        botonMedico.setBounds(295, 300, 180, 60);
        botonMedico.setFont(font);
        botonMedico.setToolTipText("Gestionar Medico");
        botonCita = new JButton("CITA");
        botonCita.setBounds(535, 300, 180, 60);
        botonCita.setFont(font);
        botonCita.setToolTipText("Gestionar Cita");
        botonAcerca = new JButton("ACERCA");
        botonAcerca.setBounds(10, 380, 120, 20);
        botonDocumentacion = new JButton("DOCUMENTACIÓN");
        botonDocumentacion.setBounds(140, 380, 150, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(590, 380, 120, 20);
        
        //imagen = new ImageIcon (img);
        etiImagen = new JLabel();
        etiImagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/consulta.png")));
        etiImagen.setBounds(180, 20, 350, 249);
        
                
        panel.add(botonPaciente);
        panel.add(botonMedico);
        panel.add(botonCita);
        panel.add(botonAcerca);
        panel.add(botonDocumentacion);
        panel.add(botonSalir);
        
        panel.add(etiImagen);
        
        ventana = new JInternalFrame();
        ventana.setTitle("Consulta Medica Privada");
        
        //ventana.setLocationRelativeTo(null);
            ventana.setSize(700,400);
        //este metodo devuelve el tamaño de la pantalla
            //Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            //Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            //ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
        
       
        
        //ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        ventana.add(panel);
        
        //ventana.setVisible(true);
       
    }
    
    public JButton getBotonPaciente() {
        return botonPaciente;
    }

    public JButton getBotonMedico() {
        return botonMedico;
    }

    public JButton getBotonCita() {
        return botonCita;
    }

    public JButton getBotonAcerca() {
        return botonAcerca;
    }

    public JButton getBotonDocumentacion() {
        return botonDocumentacion;
    }

    public JButton getBotonSalir() {
        return botonSalir;
    }

    public JInternalFrame getVentana() {
        return ventana;
    }

    public JPanel getPanel() {
        return panel;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public JLabel getEtiImagen() {
        return etiImagen;
    }

    public JLabel getEtiMenu() {
        return etiMenu;
    }
   
    
}