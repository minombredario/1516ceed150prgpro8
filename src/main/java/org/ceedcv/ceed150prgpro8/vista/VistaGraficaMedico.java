/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro8.vista;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import javax.swing.*;


/**

* Fichero: VistaGraficaMedico.java

* @author Darío Navarro Andrés <minombredario@gmail.com>

* @date 28-feb-2016

*/
public class VistaGraficaMedico extends JInternalFrame{
    
    private JLabel etiId,etiNombre, etiEdad, etiTelefono, etiObservaciones, etiNcolegiado, etiEspecialidad, etiDni, etiImagen, etiMenu;
    private JTextField txtId, txtNombre, txtEdad, txtTelefono, txtNcolegiado, txtEspecialidad, txtDni;
    private JButton botonCreate, botonUpdate, botonRead, botonDelete, botonSalir, botonAceptar, botonCancelar, botonGuardar, botonBorrar, botonPrimero, botonUltimo, botonSiguiente, botonAnterior;
    private JTextArea txtObservaciones;
    private JInternalFrame ventana;
    private JPanel panel;
    private ImageIcon imagen;

    public VistaGraficaMedico(){
       
        panel = new JPanel();
        panel.setLayout(null);
        //String img = "src/main/resources/imagenes/doctorlogo.png";
        
        etiMenu = new JLabel("MENU MEDICO");
        etiMenu.setBounds(300, 5, 300, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiNombre = new JLabel("Nombre: ");
        etiNombre.setBounds(10, 50, 80, 20);
        etiEdad = new JLabel("Edad: ");
        etiEdad.setBounds(10, 80, 80, 20);
        etiTelefono = new JLabel("Telefono: ");
        etiTelefono.setBounds(160, 80, 120, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiDni = new JLabel ("Dni: ");
        etiDni.setBounds(325, 80, 80, 20);
        etiNcolegiado = new JLabel("Nº Colegiado: ");
        etiNcolegiado.setBounds(10, 110, 80, 20);
        etiEspecialidad = new JLabel("Especialidad: ");
        etiEspecialidad.setBounds(200, 110, 80, 20);
        
        //imagen = new ImageIcon (img);
        etiImagen = new JLabel(imagen);
        etiImagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/doctorlogo.png")));
        etiImagen.setBounds(450, 22, 323, 380);
        
        
        
        txtId = new JTextField();
        txtId.setBounds(90, 20, 50, 20);
        txtNombre = new JTextField();
        txtNombre.setBounds(90, 50, 345, 20);
        txtEdad = new JTextField();
        txtEdad.setBounds(90, 80, 50, 20);
        txtTelefono = new JTextField();
        txtTelefono.setBounds(230, 80, 70, 20);
        txtDni = new JTextField();
        txtDni.setBounds(365, 80, 70, 20);
        txtNcolegiado = new JTextField();
        txtNcolegiado.setBounds(90, 110, 100, 20);
        txtEspecialidad = new JTextField();
        txtEspecialidad.setBounds(280, 110, 155, 20);
       
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 425, 150);
        
        botonCreate = new JButton("CREATE");
        botonCreate.setBounds(10, 350, 90, 20);
        botonRead = new JButton("READ");
        botonRead.setBounds(110, 350, 90, 20);
        botonUpdate = new JButton("UPDATE");
        botonUpdate.setBounds(210, 350, 90, 20);
        botonDelete = new JButton("DELETE");
        botonDelete.setBounds(310, 350, 90, 20);
        botonAceptar = new JButton("ACEPTAR");
        botonAceptar.setBounds(550, 350, 100, 20);
        botonCancelar = new JButton("CANCELAR");
        botonCancelar.setBounds(670, 350, 100, 20);
        botonSalir = new JButton("SALIR");
        botonSalir.setBounds(670, 10, 100, 20);
        botonGuardar = new JButton("GUARDAR");
        botonGuardar.setBounds(550, 350, 100, 20);
        botonGuardar.setVisible(false);
        botonBorrar = new JButton("BORRAR");
        botonBorrar.setBounds(550, 350, 100, 20);
        botonBorrar.setVisible(false);
        
        botonPrimero = new JButton("<<");
        botonPrimero.setBounds(70, 320, 50, 20);
        botonAnterior =new JButton("<");
        botonAnterior.setBounds(140, 320, 50, 20);
        botonSiguiente = new JButton(">");
        botonSiguiente.setBounds(210, 320, 50, 20);
        botonUltimo = new JButton (">>");
        botonUltimo.setBounds(280, 320, 50, 20);
        
       
        panel.add(etiId);
        panel.add(etiNombre);
        panel.add(etiDni);
        panel.add(etiEdad);
        panel.add(etiTelefono);
        panel.add(etiObservaciones);
        panel.add(txtId);
        panel.add(txtNombre);
        panel.add(txtEdad);
        panel.add(txtTelefono);
        panel.add(txtObservaciones);
        panel.add(txtDni);
        
        panel.add(botonCreate);
        panel.add(botonUpdate);
        panel.add(botonRead);
        panel.add(botonDelete);
        panel.add(botonAceptar);
        panel.add(botonSalir);
        panel.add(botonCancelar);
        panel.add(botonGuardar);
        panel.add(botonBorrar);
        
        panel.add(botonPrimero);
        panel.add(botonSiguiente);
        panel.add(botonAnterior);
        panel.add(botonUltimo);
        
                         
        panel.add(etiNcolegiado);
        panel.add(etiEspecialidad);
        panel.add(txtNcolegiado);
        panel.add(txtEspecialidad);
        
        panel.add(etiImagen);
        
        
        
        
        ventana = new JInternalFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,420);
        //este metodo devuelve el tamaño de la pantalla
            //Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            //Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            //ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
            
        //ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        Set<AWTKeyStroke> teclas = new HashSet<AWTKeyStroke>();
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_ENTER, 0));
        teclas.add(AWTKeyStroke.getAWTKeyStroke(
                KeyEvent.VK_TAB, 0));
        
        // Se pasa el conjunto de teclas al panel principal 
        ventana.getContentPane().setFocusTraversalKeys(
                KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, 
                teclas);
        ventana.add(panel);
        
        //ventana.setVisible(true);
        
    }
    
           
   /*public static void main(String[] args) {
        VistaGraficaMedico principal = new VistaGraficaMedico();
    }*/

    public JLabel getEtiId() {
        return etiId;
    }

    public JLabel getEtiNombre() {
        return etiNombre;
    }

    public JLabel getEtiEdad() {
        return etiEdad;
    }

    public JLabel getEtiTelefono() {
        return etiTelefono;
    }

    public JLabel getEtiObservaciones() {
        return etiObservaciones;
    }

    public JLabel getEtiNcolegiado() {
        return etiNcolegiado;
    }

    public JLabel getEtiEspecialidad() {
        return etiEspecialidad;
    }

    public JLabel getEtiDni() {
        return etiDni;
    }

    public JLabel getEtiImagen() {
        return etiImagen;
    }

    public JLabel getEtiMenu() {
        return etiMenu;
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public JTextField getTxtNombre() {
        return txtNombre;
    }

    public JTextField getTxtEdad() {
        return txtEdad;
    }

    public JTextField getTxtTelefono() {
        return txtTelefono;
    }

    public JTextField getTxtNcolegiado() {
        return txtNcolegiado;
    }

    public JTextField getTxtEspecialidad() {
        return txtEspecialidad;
    }

    public JTextField getTxtDni() {
        return txtDni;
    }

    public JButton getBotonCreate() {
        return botonCreate;
    }

    public JButton getBotonUpdate() {
        return botonUpdate;
    }

    public JButton getBotonRead() {
        return botonRead;
    }

    public JButton getBotonDelete() {
        return botonDelete;
    }

    public JButton getBotonSalir() {
        return botonSalir;
    }

    public JButton getBotonAceptar() {
        return botonAceptar;
    }

    public JButton getBotonCancelar() {
        return botonCancelar;
    }

    public JButton getBotonGuardar() {
        return botonGuardar;
    }

    public JButton getBotonBorrar() {
        return botonBorrar;
    }

    public JButton getBotonPrimero() {
        return botonPrimero;
    }

    public JButton getBotonUltimo() {
        return botonUltimo;
    }

    public JButton getBotonSiguiente() {
        return botonSiguiente;
    }

    public JButton getBotonAnterior() {
        return botonAnterior;
    }

    public JTextArea getTxtObservaciones() {
        return txtObservaciones;
    }

    public JInternalFrame getVentana() {
        return ventana;
    }

    public JPanel getPanel() {
        return panel;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

   
}
