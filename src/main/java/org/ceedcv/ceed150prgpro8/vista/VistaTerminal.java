package org.ceedcv.ceed150prgpro8.vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Fichero: VistaTerminal.java
 * 
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 21-oct-2015
 */

public class VistaTerminal {
    public String Horas[];
    
    public int pedirInt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea;
        int numero = 0;
        try{
             linea = buffer.readLine();
            numero = Integer.parseInt(linea);
        }catch (Exception e){
            System.err.println("Introduce un valor númerico");
            
        }
        return numero;

    }

    public String pedirString() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea = null;

        try {
            linea = buffer.readLine();
        } catch (Exception e) {
            System.err.println("Introduce texto");
        }
        return linea;
    }

    public void Esperar() throws IOException {
        Scanner continuar = new Scanner(System.in);
        System.out.println("Pulsa cualquier tecla para continuar");
        continuar.nextLine();
    }
           
    //Función para mostrar un texto por pantalla
    public void mostrarTexto(String texto) {
        System.out.print(texto);
    }
    
    
    //Función para leer una letra por teclado y pasarla a minúscula
    public static char leerLetra() {
        String linea;
        char opcion;
        Scanner sc = new Scanner (System.in);
        linea = sc.next().toLowerCase();
        opcion = linea.charAt(0);
        return opcion;
    }
   /*
    0-> Error
    1-> Informacion
    2-> Avertencia
    3-> prgunta
    */
    public static JOptionPane info(String texto) {
        JOptionPane info = new JOptionPane();
        info.showMessageDialog(null, texto, "Información", 1);
        return info;
    }
    public static JOptionPane advertencia(String texto) {
        JOptionPane atencion = new JOptionPane();
        atencion.showMessageDialog(null, texto, "Atencion", 2);
        return atencion;
    }
    public static JOptionPane info2(Object texto, String titulo, ImageIcon logo) {
        JOptionPane info = new JOptionPane();
        //logo.
        info.showMessageDialog(null, texto, titulo, 1, logo);
        return info;
    }
      
    public static JOptionPane error(String texto) {
        JOptionPane error = new JOptionPane();
        error.showMessageDialog(null, texto, "Error", 0);
        return error;
    }
    
    
    public String ConvertirDatetoSTring(Date fecha){
        String sfecha = null;
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");

        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd");
        sfecha = fechaHora.format(fecha);

        return sfecha;
    }
    
    public void Horas(){
        int h;
        int m;
        for(int i = 0; i < 1; i++){
            for( h = 7; h <= 14;h++){
                for( m = 00; m < 60; m= m + 15){
                   Horas[i] = h + ":" + m;
                   
                }

            }
        }
        
    }
}

