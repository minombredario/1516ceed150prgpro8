/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro8.vista;

import java.awt.*;
import static javafx.scene.paint.Color.color;
import javax.swing.*;


/**
 *
 * @author usu1601
 */
public class VistaGraficaAcerca extends JInternalFrame{

    private JLabel etiAcerca1, etiAcerca2, etiAcerca3, etiAcerca4, etiAcerca5;
    
    private JButton botonSalir;
   
    private JInternalFrame ventana;
    private JPanel panel;
    //private ImageIcon imagen;

    public VistaGraficaAcerca(){
                
        Font font = new Font ("helvetica" , Font.BOLD, 15);
        panel = new JPanel();
        panel.setLayout(null);
        //Color color = new Color(255,255,255,150);
        //String img = "src/main/java/Imagenes/fondo.png";
        
        etiAcerca1 = new JLabel("Aplicación de ejemplo.");
        etiAcerca2 = new JLabel("Daw1.Programación.Tema 8 IGU.");
        etiAcerca3 = new JLabel("Por Darío Navarro Andrés.");
        etiAcerca4 = new JLabel("13/Mar/2016");
        etiAcerca5 = new JLabel("minombredario@gmail.com.");
        
        etiAcerca1.setBounds(280, 50, 250, 35);
        etiAcerca2.setBounds(280, 100, 250, 35);
        etiAcerca3.setBounds(280, 150, 250, 35);
        etiAcerca4.setBounds(280, 200, 250, 35);
        etiAcerca5.setBounds(280, 250, 250, 35);
        
       /* para cambiar el color de la etiqueta
        etiAcerca1.setBackground(color);
        etiAcerca1.setOpaque(true);
        etiAcerca2.setBackground(color);
        etiAcerca2.setOpaque(true);
        etiAcerca3.setBackground(color);
        etiAcerca3.setOpaque(true);
        etiAcerca4.setBackground(color);
        etiAcerca4.setOpaque(true);
        etiAcerca5.setBackground(color);
        etiAcerca5.setOpaque(true);*/
        
        etiAcerca1.setFont(font);
        etiAcerca2.setFont(font);
        etiAcerca3.setFont(font);
        etiAcerca4.setFont(font);
        etiAcerca5.setFont(font);
       
        
        botonSalir = new JButton ("Salir");
        botonSalir.setFont(font);
        botonSalir.setBounds(300, 320, 90, 30);
        
        panel.add(etiAcerca1);
        panel.add(etiAcerca2);
        panel.add(etiAcerca3);
        panel.add(etiAcerca4);
        panel.add(etiAcerca5);
        
        panel.add(botonSalir);
        //panel.setBackground(new Color(30,70,210));
        //panel.setOpaque(true);
        /*ImageIcon imagen = new ImageIcon (img);
        JLabel etiImagen = new JLabel(imagen);
        etiImagen.setBounds(0, 0, 323, 380);
        panel.add(etiImagen);*/
        
        ventana = new JInternalFrame();
        ventana.setTitle("ACERCA DE");
        ventana.setSize(700,400);
        //este metodo devuelve el tamaño de la pantalla
            //Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            //Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            //ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
            
        //ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        ventana.add(panel);
        
        //ventana.setVisible(true);
        
    }
    
   /*public static void main(String[] args) {
        VistaGraficaAcerca principal = new VistaGraficaAcerca();
    }*/

    public JLabel getEtiAcerca1() {
        return etiAcerca1;
    }

    public JLabel getEtiAcerca2() {
        return etiAcerca2;
    }

    public JLabel getEtiAcerca3() {
        return etiAcerca3;
    }

    public JLabel getEtiAcerca4() {
        return etiAcerca4;
    }

    public JLabel getEtiAcerca5() {
        return etiAcerca5;
    }

    public JButton getBotonSalir() {
        return botonSalir;
    }

    public JInternalFrame getVentana() {
        return ventana;
    }

    public JPanel getPanel() {
        return panel;
    }
    
}
