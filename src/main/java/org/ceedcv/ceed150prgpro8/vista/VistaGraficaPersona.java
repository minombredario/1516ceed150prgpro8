/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro8.vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.ceedcv.ceed150prgpro8.modelo.Persona;

/**

* Fichero: VistaGraficaPersona.java

* @author Darío Navarro Andrés <minombredario@gmail.com>

* @date 28-feb-2016

*/
public class VistaGraficaPersona {
    private JLabel etiId,etiNombre, etiEdad, etiDni, etiTelefono, etiObservaciones, etiMenu;
    private JTextField txtId, txtNombre, txtEdad, txtTelefono, txtDni;
    private JButton botonEnviar;
    private JTextArea txtObservaciones;
    private JFrame ventana;
    private JPanel panel;

    public void CrearMenuPersona(){
       
        panel = new JPanel();
        panel.setLayout(null);
        String img = "src/main/java/Imagenes/familia.png";
        
        etiMenu = new JLabel("MENU PERSONA");
        etiMenu.setBounds(220, 5, 250, 35);
        etiMenu.setFont(new Font("Helvetica", Font.BOLD, 30));
        etiMenu.setForeground(Color.LIGHT_GRAY);
        //etiMenu.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
        panel.add(etiMenu);
        etiId = new JLabel("Id: ");
        etiId.setBounds(10, 20, 80, 20);
        etiNombre = new JLabel("Nombre: ");
        etiNombre.setBounds(10, 50, 80, 20);
        etiEdad = new JLabel("Edad: ");
        etiEdad.setBounds(10, 80, 80, 20);
        etiTelefono = new JLabel("Telefono: ");
        etiTelefono.setBounds(10, 110, 80, 20);
        etiObservaciones = new JLabel("Observaciones: ");
        etiObservaciones.setBounds(10, 140, 100, 20);
        
        etiDni = new JLabel ("Dni: ");
        etiDni.setBounds(200, 50, 80, 20);
        
        txtId = new JTextField();
        txtId.setBounds(75, 20, 80, 20);
        txtNombre = new JTextField();
        txtNombre.setBounds(75, 50, 80, 20);
        txtEdad = new JTextField();
        txtEdad.setBounds(75, 80, 80, 20);
        txtTelefono = new JTextField();
        txtTelefono.setBounds(75, 110, 80, 20);
        txtDni = new JTextField();
        txtDni.setBounds(320, 50, 80, 20);
        
        txtObservaciones = new JTextArea();
        txtObservaciones.setBounds(10, 160, 390, 150);
        
       
        panel.add(etiId);
        panel.add(etiNombre);
        panel.add(etiDni);
        panel.add(etiEdad);
        panel.add(etiTelefono);
        panel.add(etiObservaciones);
        panel.add(txtId);
        panel.add(txtNombre);
        panel.add(txtEdad);
        panel.add(txtTelefono);
        panel.add(txtObservaciones);
        panel.add(txtDni);
        
        
        
        ventana = new JFrame();
        ventana.setTitle("Consulta Medica Privada");
        ventana.setSize(700,400);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = ventana.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
            ventana.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
        ventana.setDefaultCloseOperation(ventana.EXIT_ON_CLOSE);
        ventana.add(panel);
        
        //ventana.setVisible(true);
    }
    
    /*public VistaGraficaPersona(){
        CrearMenuPersona();
        
    }
    
    public static void main(String[] args) {
        VistaGraficaPersona principal = new VistaGraficaPersona();
    }
    
    */
    
}
