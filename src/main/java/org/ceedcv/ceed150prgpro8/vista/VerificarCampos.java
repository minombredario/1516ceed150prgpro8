/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro8.vista;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
* Fichero: VerificarCampos.java
* @author Darío Navarro Andrés <minombredario@gmail.com>
* @date 07-mar-2016
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class VerificarCampos extends InputVerifier {
  
    private String variable;
    VistaTerminal vt;
  
    public VerificarCampos(String variable){
        this.variable = variable;
    }
    
    public static boolean validarDni(String dni){
        
        
        Pattern dniPattern = Pattern.compile("(\\d{1,8}[a-z])");
	Matcher m = dniPattern.matcher(dni);
        
        String numerodni = dni.substring(0,(dni.length()-1));
        char letra_ = dni.toUpperCase().charAt(8);
        int num = Integer.parseInt(numerodni) % 23;
        char letra = "TRWAGMYFPDXBNJZSQVHLCKET".charAt(num);
        String completo = numerodni + letra;
        
	if(m.matches() && (letra !=(letra_))){
                    
            return false;
	} else 
		return true;
      
    }
     
    private boolean validarNumero(String texto){
       
        try {
            int i = Integer.parseInt(texto);
        }catch (Exception e){
            int i;
            return false;
        }
        return true;
    }
    
    private boolean validarTelefono(String telefono){
       Pattern telefonoPattern = Pattern.compile ("^[9|8|7|6]\\d{8}$");
       Matcher m = telefonoPattern.matcher(telefono);
        
	if(m.matches()){
           return true;
	} else 
		return false; 
    }
    
    private boolean validarNcolegiado(String ncolegiado){
        //08/08/1234
        Pattern ncolegiadoPattern = Pattern.compile ("[0-9]{2}/[0-9]{2}/[0-9]{4}");
	Matcher m = ncolegiadoPattern.matcher(ncolegiado);
        
	if(m.matches()){
           return true;
	} else 
		return false;
    }
    private boolean validarNss(String nss){
        //08/08/1234
        Pattern nssPattern = Pattern.compile ("[0-9]{2}/[0-9]{7,8}/[0-9]{2}");
	Matcher m = nssPattern.matcher(nss);
        
	if(m.matches()){
           return true;
	} else 
		return false;
    }
    private boolean validarHora(String hora){
        
        Pattern nssPattern = Pattern.compile ("[0-2][0-9]:[0-5][0-9]");
	Matcher m = nssPattern.matcher(hora);
        int hora_ = Integer.parseInt(hora.substring(0,2));
        
	if(m.matches()&& hora_ < 24){
           return true;
	} else 
		return false; 
    }
  
  public boolean verify(JComponent input)
  {
    if ((input instanceof JTextField))
    {
      String texto = ((JTextField)input).getText();
      switch (this.variable)
      {
      case "txtEdad": 
            if (!validarNumero(texto)){
                vt.advertencia("Error en Edad");
                return false;
            }
            break;
      case "txtDni": 
            if (!validarDni(texto)){
                vt.advertencia("Dni incorrecto\nFormato " + "'123456789Y'");
                return false;
            }
            break;
      case "txtNcolegiado":
            if(!validarNcolegiado(texto)){
                vt.advertencia( "Nº colegiado incorrecto\nFormato xx/xx/xxxx");  
                return false;
            }
            break;
      case "txtTelefono":
            if(!validarTelefono(texto)){
                vt.advertencia("Nº telefono incorrecto\nEmpieza por (6-7-9) seguido de 8 digitos");  
                return false;
            }
            break;
      case "txtNombre":
          if(validarNumero(texto)){
                vt.advertencia( "Solo Texto");  
                return false;
            }
            break;
      case "txtEspecialidad":
          if(validarNumero(texto)){
                vt.advertencia("Solo Texto");  
                return false;
            }
            break;
      case "txtNss":
          if(!validarNss(texto)){
                vt.advertencia("Nº Seguridad Social incorrecto\nxx/xxxxxxxx/xx\nxx/xxxxxxx/xx");  
                return false;
            }
            break;
      case "hora":
          if(!validarHora(texto)){
               vt.advertencia("Formato de fecha xx:xx");  
                return false;
            }
          break;
      }
    }
    return true;
  }
}
